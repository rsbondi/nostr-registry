const sqlite3 = require('sqlite3').verbose()

class Database {
  constructor() {
    this.db = new sqlite3.Database(`${__dirname}/../relays.sqlite`);
  }

  addRelay(url, relay) {
    return new Promise((resolve, reject) => {
      const {nips, created, lastseen, lastdown, uptime} = relay
      const sql = this.db.prepare("INSERT INTO relays (url, nips, created, lastseen, lastdown, uptime) VALUES(?, ?, ?, ?, ?, ?)")
      sql.run(url, nips, created, lastseen, lastdown, uptime, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      }).finalize()
    })
  }

  updateRelay(url, relay) {
    return new Promise((resolve, reject) => {
      const {lastseen, lastdown, uptime} = relay
      const sql = this.db.prepare("UPDATE relays SET lastseen=?, lastdown=?, uptime=? WHERE url=?")
      sql.run(lastseen, lastdown, uptime, url, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      }).finalize()
    })
  }

  getRelays() {
    return new Promise((resolve, reject) => {
      this.db.all('SELECT * FROM relays', (err, rows) => {
        if (rows) {
          resolve(rows)
        } else {
          reject(err.message)
        }
      })
    })
  }
}

module.exports = new Database()
