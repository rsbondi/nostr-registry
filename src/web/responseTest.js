import {relayPool, getPublicKey} from 'nostr-tools' 

const privkey = "BE711479A0FC504E04545323CE88F89D456B804CE6948B8172637E02424A9560"
const pubkey = getPublicKey(privkey)
let relays = {}
const pool = relayPool()
let ids = []
let testIndex
const testResultList = document.querySelector('#responseResult')
const runTestButton = document.querySelector('#responseTest')
const attempts = {}
const progress = document.querySelector('#progress')

export function setRelays(r) {
  relays = r
}

function urls() {
  return Object.keys(relays)
}

export async function initTest() {
  pool.setPrivateKey(privkey)
  
  function onevent(url, event) {
    console.log(`got a response from ${url} with event.`, event)
    const li = document.createElement('li')
    const delay = (event.tags.length && 
                   event.tags[0][0] === "t" && 
                   (new Date()).getTime() - event.tags[0][1]) || 
                   ""
    li.innerHTML = `response in <strong>${delay}</strong> ms from ${url}`
    li.id = url
    testResultList.appendChild(li);
    event.delay = delay
    event.url = url
    clearTimeout(attempts[url])
    testIndex ++
    testOne()
    ids.push(event)
  }

  pool.onNotice((message, relay) => {
    const li = document.createElement('li')
    li.innerHTML = `<strong>${message}</strong> from ${relay.url}`
    testResultList.appendChild(li);
    testIndex ++
    testOne()
  })

  function reset() {testResultList.innerHTML = ""; ids = [] }


  function sortReays() {
    ids.sort((a,b) => a.delay - b.delay)
    ids.forEach((id, i) => {
      const node = document.getElementById(id.url)
      testResultList.insertBefore(node, testResultList.childNodes[i])
    })
  }

  function listMessage(msg) {
    const li = document.createElement('li')
    li.textContent = msg
    testResultList.appendChild(li);
    testIndex ++
    testOne()  
  }

  async function testOne() {
    progress.className ='running'
    if (testIndex > 0) await pool.removeRelay(urls()[testIndex - 1])
    if (testIndex >= urls().length) {
      sortReays()
      progress.className ='idle'
      runTestButton.removeAttribute('disabled')
      return
    }
    const url = urls()[testIndex]
    const relay = await pool.addRelay(url, {read: true, write: true})
    if (!relays[url].connected) {
      listMessage(`skipping offline relay ${url}`)
      return
    }
    // await relay.sub({cb: ev => onevent(url, ev), filter: {author:pubkey}})
    const created = new Date().getTime()
    const content = {name: "test bot", about: "performance test", picture: ""}
    const event = {
      pubkey,
      created_at: Math.round(created / 1000),
      kind: 0,
      content: JSON.stringify(content),
      tags: [["t", created, ""]]
    }
    try {
      const ev = await pool.publish(event, (status, url) => {
        if (status === 1) {
          onevent(url, ev)
        }
      })
      attempts[url] = setTimeout(() => {
        listMessage(`timeout testing ${url}`)
      }, 3000)
    } catch (e) {
      listMessage(`error publishing to ${url}`)
    }

  }

  runTestButton.addEventListener('click', async e => {
    console.log('click')
    runTestButton.setAttribute('disabled', 'disabled')
    e.preventDefault()
    reset()
    testIndex = 0
    testOne()
  })

}

