const Monitor = require('./monitor')
const express = require('express')
const https = require('https')
const fs = require('fs')
const cors = require('cors')

const {CERT, PRIV_KEY} = process.env
const privateKey = fs.readFileSync(PRIV_KEY)
const certificate = fs.readFileSync(CERT)
const credentials = { key: privateKey, cert: certificate }

const server = express()
server.use(express.json())
server.use(cors())
const port = 3101

const monitor = new Monitor()

// server.post('/addrelay', (req, res) => {
//   const { url, nips } = req.body
//   monitor.addRelay(url, nips)
//     .then(relay => res.send(relay))
//     .catch(e => {
//       res.status(500)
//       res.send(JSON.stringify({error: e}))
//     })
// })

server.get('/getrelays', (req, res) => {
  res.send(JSON.stringify(monitor.getRelays()))
})

const httpsServer = https.createServer(credentials, server)

httpsServer.listen(port, () => {
  console.log(`Registry app listening at http://localhost:${port}`)
})
